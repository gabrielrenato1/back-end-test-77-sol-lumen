<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Guzzle\Http\Client;

class SimulatorController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }


     function index(Request $request){
       $this->validate($request, [
           'cep' => 'required|string|min:8|max:9',
           'account_value' => 'required',
           'type' => 'required|string',
       ]);

       $cep = preg_replace("/[^0-9]/", "",$request->cep);
       $urlCepApi = 'https://viacep.com.br/ws/'.$cep.'/json/';
       $addressResponse = json_decode(file_get_contents($urlCepApi));
       $apiKey = 'AIzaSyAvuDreKNoMOdRv4-5aXD5awaC3pB_sKqU';
       $urlGeoGoogleApi = 'https://maps.googleapis.com/maps/api/geocode/json?address='.str_replace(" ", "+", $addressResponse->logradouro).','.str_replace(" ", "+", $addressResponse->localidade).','.$addressResponse->uf.'&key='.$apiKey;

       $googleLocationResponse = json_decode(file_get_contents($urlGeoGoogleApi))->results[0]->geometry->location;

       $urlsolApi = 'https://api2.77sol.com.br/busca-cep?estrutura='.$request->type.'&estado='.$addressResponse->uf.'&cidade='.str_replace(" ", "%20", $addressResponse->localidade).'&valor_conta='.$request->account_value.'&cep='.$addressResponse->cep.'&latitude='.number_format($googleLocationResponse->lat, 1, '.', '').'&longitude='.number_format($googleLocationResponse->lng, 1, '.', '');

       $client = new \GuzzleHttp\Client();
       $response = $client->request('GET', $urlsolApi);

       return response($response->getBody());
     }
}
